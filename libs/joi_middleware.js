const Joi = require("@hapi/joi");

const middleware = function (schema, testBody = true) {
    return function (req, res, next) {
        let dataForValidationBody = req.body;
        let dataForValidationParams = req.params;
        let error = null;

        if(testBody) {
            error = schema.validate(dataForValidationBody).error || null;
        } else {
            error = schema.validate(dataForValidationParams).error || null;
        }

        const valid = error == null;

        if (valid) {
            console.log('valid');
            next();
        } else {
            const { details } = error;
            const message = details.map(i => i.message).join(',');

            next({status: 422, message: message});
        }
    }
};

module.exports = middleware;