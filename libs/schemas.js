const Joi = require('@hapi/joi');

const positiveIntegerNumberRequiredValidator = Joi.number()
    .integer()
    .positive()
    .required();

const positiveIntegerNumberValidator = Joi.number()
    .positive()
    .integer();

const schemas = {
    orderCreate: Joi.object({
        id: positiveIntegerNumberValidator,
        customerId: positiveIntegerNumberRequiredValidator,
        statusId: positiveIntegerNumberValidator,
        date: Joi.date().iso()
    }),
    orderGet: Joi.object({
        id: positiveIntegerNumberValidator,
        customerId: positiveIntegerNumberValidator,
        statusId: positiveIntegerNumberValidator,
        date: Joi.date().iso()
    }),

    orderItemCreate: Joi.object({
        id: positiveIntegerNumberValidator,
        orderId: positiveIntegerNumberRequiredValidator,
        pizzaId: positiveIntegerNumberRequiredValidator,
        sizeId: positiveIntegerNumberRequiredValidator,
        quantity: positiveIntegerNumberRequiredValidator
    }),

    orderItemGet: Joi.object({
        id: positiveIntegerNumberValidator,
        orderId: positiveIntegerNumberValidator,
        pizzaId: positiveIntegerNumberValidator,
        sizeId: positiveIntegerNumberValidator,
        quantity: positiveIntegerNumberValidator
    })
};

module.exports = schemas;