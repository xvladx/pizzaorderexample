var models = require("../models");
var express = require('express');
var router = express.Router();
var orderItemsRouter = require('./order_items');
const joiMiddleware = require('../libs/joi_middleware');
const schemas = require('../libs/schemas');

router.post('/', joiMiddleware(schemas.orderCreate), async (req, res, next) => {
    delete req.body.id;

    return models.models.Status.primaryStatus().then(status => {
        if(typeof req.body.statusId == "undefined") {
            req.body.statusId = status.id;
        }
        return models.models.Order.create(req.body);
    }).then(order => {
        res.send(order);
    }).catch(next);
});

router.get('/', joiMiddleware(schemas.orderGet), async (req, res, next) => {
    let where = req.body;

    return models.models.Order.findAll({where: where}).then(orders => {
        res.send(orders);
    }).catch(next);
});

router.get('/:id', joiMiddleware(schemas.orderGet, false), async (req, res, next) => {
    return models.models.Order.findByPk(req.params.id).then(order => {
        if(order) {
            res.send(order);
        } else {
            next({status: 404});
        }
    }).catch(next);
});

router.delete('/:id', joiMiddleware(schemas.orderGet, false), async (req, res, next) => {
    return models.sequelize.transaction().then(transaction => {
        return models.models.OrderItem.destroy({where: {orderId: req.params.id}, transaction}).then(number => {
            return models.models.Order.destroy({where: {id: req.params.id}, transaction}).then(number => {
                transaction.commit();
                res.send({
                    count: number
                });
            }).catch(reason => {
                transaction.rollback();
                next(reason);
            });
        }).catch(reason => {
            transaction.rollback();
            next(reason);
        })
    });
});

router.put('/:id', joiMiddleware(schemas.orderGet, false), joiMiddleware(schemas.orderGet), async (req, res, next) => {
    delete req.body.id;

    return models.models.Order.findByPk(req.params.id).then(order => {
        //Was horribly tired and had absolutely no time to implement it in a good way...
        if(order.statusId === 4) {
            req.body.statusId = 4;
        }

        //Why would we need to get order from db twice?
        Object.keys(req.body).forEach(function(key) {
            let val = req.body[key];
            if(typeof order[key] != "undefined") {
                order[key] = val;
            }
        });
        return models.models.Order.update(req.body, {where: {id: req.params.id}, returning: true}).then(numChanged => {
            if(Array.isArray(numChanged) && numChanged[0] > 0) {
                res.send(order);
            }
        }).catch(next);
    }).catch(next);
});

router.use('/:id/items', joiMiddleware(schemas.orderGet, false), async (req, res, next) => {
    req._orderId = req.params.id;
    next();
}, orderItemsRouter);

module.exports = router;
