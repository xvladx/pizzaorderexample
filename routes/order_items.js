var models = require("../models");
var express = require('express');
var router = express.Router();
const joiMiddleware = require('../libs/joi_middleware');
const schemas = require('../libs/schemas');

router.post('/', joiMiddleware(schemas.orderItemCreate), async (req, res, next) => {
    delete req.body.id;

    if(typeof req._orderId != "undefined") {
        req.body.orderId = req._orderId;
    }

    return models.models.OrderItem.create(req.body).then(orderItem => {
        res.send(orderItem);
    }).catch(next);
});

router.get('/', joiMiddleware(schemas.orderItemGet), async (req, res, next) => {
    if(typeof req._orderId != "undefined") {
        req.body.orderId = req._orderId;
    }
    return models.models.OrderItem.findAll({where: req.body}).then(orderItems => {
        res.send(orderItems);
    }).catch(next);
});

router.get('/:id', joiMiddleware(schemas.orderItemGet, false), async (req, res, next) => {
    let where = {
        id: req.params.id
    };
    if(typeof req._orderId != "undefined") {
        where.orderId = req._orderId;
    }

    return models.models.OrderItem.findOne({where: where}).then(orderItem => {
        if(orderItem) {
            res.send(orderItem);
        } else {
            next({status: 404});
        }
    }).catch(next);
});

router.delete('/:id', joiMiddleware(schemas.orderItemGet, false), async (req, res, next) => {
    let where = {
        id: req.params.id
    };
    if(typeof req._orderId != "undefined") {
        where.orderId = req._orderId;
    }

    return models.models.OrderItem.destroy({where: where}).then(number => {
        res.send({
            count: number
        });
    }).catch(next);
});

router.put('/:id', joiMiddleware(schemas.orderItemGet, false), joiMiddleware(schemas.orderItemGet), async (req, res, next) => {
    delete req.body.id;
    delete req.body.orderId;

    let where = {
        id: req.params.id
    };
    if(typeof req._orderId != "undefined") {
        where.orderId = req._orderId;
    }

    return models.models.OrderItem.update(req.body, {where: where}).then(order => {
        return models.models.OrderItem.findOne({where: where}).then(orderItem => {
            res.send(orderItem);
        });
    }).catch(next);
});

module.exports = router;