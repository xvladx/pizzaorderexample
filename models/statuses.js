/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Status = sequelize.define('statuses', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'statuses',
    timestamps: false
  });

  Status.primaryStatus = () => {
    return Status.findOne({
      order: [["order", "ASC"]]
    });
  };

  return Status;
};
