/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pizzas_sizes', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    pizzaId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'pizzas',
        key: 'id'
      }
    },
    sizeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'sizes',
        key: 'id'
      }
    }
  }, {
    tableName: 'pizzas_sizes',
    timestamps: false
  });
};
