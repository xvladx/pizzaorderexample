var sq = require('sequelize');

const sequelize = new sq.Sequelize(process.env.DATABASE_URL, {
    dialect: 'postgres',
});

const models = {
    Pizza: sequelize.import('./pizzas'),
    Size: sequelize.import('./sizes'),
    PizzaSize: sequelize.import('./pizzas_sizes'),
    Customer: sequelize.import('./customers'),
    OrderItem: sequelize.import('./order_items'),
    Order: sequelize.import('./orders'),
    Status: sequelize.import('./statuses'),
};

models.Order.hasMany(models.OrderItem);

Object.keys(models).forEach(key => {
    if ('associate' in models[key]) {
        models[key].associate(models);
    }
});

module.exports = {
    sequelize: sequelize,
    models: models
};