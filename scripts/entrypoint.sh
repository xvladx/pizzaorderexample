#!/bin/sh
HOST=db
PORT=5432
RETRIES=10

until (echo > /dev/tcp/$HOST/$PORT) > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
    echo "Waiting for Postgres server, $((RETRIES--)) remaining attempts..."
        sleep 1
    done
npm run migrate up

if [ "$NODE_ENV" == "production" ] ; then
  npm run start
else
  npm run start:dev
fi