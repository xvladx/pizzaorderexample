var expect  = require('chai').expect;
var request = require('request');

const nonExistentOrderId = 99999;
const incorrectOrderId = "3ds34";
let createdOrderId = null;
let createdOrderItemId = null;
let createdOrderIdForTest = null;
let createdOrderIdForTest2 = null;

function testOrderRecord(falseOrderId, customerId, response, body, date = null) {
    expect(response.statusCode).to.equal(200);
    expect(body.date).to.be.a("string");

    if (date == null) {
        expect(body.date).to.equal(getDate());
    } else {
        expect(body.date).to.equal(date);
    }
    expect(body.id).to.not.equal(falseOrderId);
    expect(body.customerId).to.equal(customerId);
}

function testOrderItemRecord(falseOrderItemId, itemObject, response, obj) {
    expect(response.statusCode).to.equal(200);
    expect(obj.id).to.not.equal(falseOrderItemId);
    expect(obj.orderId).to.equal(itemObject.orderId);
    expect(obj.pizzaId).to.equal(itemObject.pizzaId);
    expect(obj.sizeId).to.equal(itemObject.sizeId);
    expect(obj.quantity).to.equal(itemObject.quantity);
}

function getDate(date = null) {
    let today = null;
    if(date == null) {
        today = new Date();
    } else {
        today = date;
    }
    let dd = today.getDate();

    let mm = today.getMonth()+1;
    let yyyy = today.getFullYear();
    if(dd<10)
    {
        dd='0'+dd;
    }

    if(mm<10)
    {
        mm='0'+mm;
    }

    return yyyy+"-"+mm+"-"+dd;
}

function getObjectFromBody(body) {
    let obj = null;
    if(typeof body == "string") {
        obj = JSON.parse(body);
    } else {
        obj = body;
    }

    return obj;
}

function testEmptyOkResponse(response, body) {
    let obj = getObjectFromBody(body);
    expect(response.statusCode).to.equal(200);
    expect(obj.length).to.equal(0);
}

describe('Orders', function() {
    before(function (done) {
        request.post({
            url: 'http://localhost:3000/orders',
            json: {
                customerId: 2,
                statusId: 4
            }
        }, function (error, response, body) {
            createdOrderIdForTest2 = body.id;
            done();
        });
    });

    it('Get all orders (may be empty)', function (done) {
        request('http://localhost:3000/orders', function (error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it('Get not existing order', function (done) {
        request('http://localhost:3000/orders/'+nonExistentOrderId, function (error, response, body) {
            expect(response.statusCode).to.equal(404);
            expect(body.length).to.equal(0);
            done();
        });
    });

    it('Get incorrect order', function (done) {
        request('http://localhost:3000/orders/'+incorrectOrderId, function (error, response, body) {
            let obj = getObjectFromBody(body);
            expect(response.statusCode).to.equal(422);
            expect(body.length).to.not.equal(0);
            expect(obj.errorObject.message).to.equal("\"id\" must be a number");
            done();
        });
    });

    it('Save new order and get it', function (done) {
        const falseId = 999999;
        const testCustomer = 1;
        let order = {
            id: falseId,
            customerId: testCustomer
        };
        request.post({
            url: 'http://localhost:3000/orders',
            json: order
        }, function (error, response, body) {
            let obj = getObjectFromBody(body);
            testOrderRecord(falseId, testCustomer, response, obj);

            createdOrderId = body.id;

            request('http://localhost:3000/orders/'+body.id, function (errorNext, responseNext, bodyNext) {
                let obj = getObjectFromBody(bodyNext);
                testOrderRecord(falseId, testCustomer, responseNext, obj);
                done();
            });
        });
    });

    it('Delete order', function (done) {
        request.delete('http://localhost:3000/orders/'+createdOrderId, function (error, response, body) {
            let obj = null;
            if(typeof body == "string") {
                obj = JSON.parse(body);
            } else {
                obj = body;
            }

            expect(response.statusCode).to.equal(200);
            expect(obj.count).to.equal(1);
            done();
        });
    });

    it('Update created order', function (done) {
        const falseId = 999999;
        const testCustomer = 1;

        const newFalseId = 88888;
        const newCustomer = 2;

        let testDate = new Date();
        testDate.setDate(1);
        testDate.setFullYear(1985);

        let stringNewDate = getDate(testDate);

        let order = {
            id: falseId,
            customerId: testCustomer
        };

        let updatedOrder = {
            id: newFalseId,
            customerId: newCustomer,
            statusId: 3,
            date: stringNewDate
        };
        request.post({
            url: 'http://localhost:3000/orders',
            json: order
        }, function (error, response, body) {
            let obj = getObjectFromBody(body);
            testOrderRecord(falseId, testCustomer, response, obj);

            createdOrderId = obj.id;

            request.put({
                url: 'http://localhost:3000/orders/'+createdOrderId,
                json: updatedOrder
            }, function (errorNext, responseNext, bodyNext) {
                let obj = getObjectFromBody(bodyNext);
                testOrderRecord(newFalseId, newCustomer, responseNext, obj, stringNewDate);
                expect(obj.statusId).to.equal(updatedOrder.statusId);
                done();
            });
        });
    });

    it('Try to update status (must fail)', function (done) {
        let updatedOrder = {
            statusId: 2
        };

        request.put({
            url: 'http://localhost:3000/orders/'+createdOrderIdForTest2,
            json: updatedOrder
        }, function (errorNext, responseNext, bodyNext) {
            let obj = getObjectFromBody(bodyNext);
            expect(responseNext.statusCode).to.equal(200);
            expect(obj.statusId).to.equal(4);
            done();
        });
    });
});

describe('Order items', function() {
    before(function (done) {
        request.post({
            url: 'http://localhost:3000/orders',
            json: {
                customerId: 2
            }
        }, function (error, response, body) {
            createdOrderIdForTest = body.id;
            done();
        });
    });

    it('Get all order items (empty)', function (done) {
        request('http://localhost:3000/orders/'+createdOrderId+'/items', function (error, response, body) {
            testEmptyOkResponse(response, body);
            done();
        });
    });

    it('Get not existing order\'s items (empty)', function (done) {
        request('http://localhost:3000/orders/'+nonExistentOrderId+'/items', function (error, response, body) {
            testEmptyOkResponse(response, body);
            done();
        });
    });

    it('Get incorrect order\'s items', function (done) {
        request('http://localhost:3000/orders/'+incorrectOrderId, function (error, response, body) {
            let obj = getObjectFromBody(body);
            expect(response.statusCode).to.equal(422);
            expect(body.length).to.not.equal(0);
            expect(obj.errorObject.message).to.equal("\"id\" must be a number");
            done();
        });
    });

    it('Add order\'s item (everithing correct)', function (done) {
        const falseId = 999999;

        let orderItem = {
            id: falseId,
            orderId: createdOrderId,
            pizzaId: 1,
            sizeId: 1,
            quantity: 2
        };

        request.post({
            url: 'http://localhost:3000/orders/'+createdOrderId+'/items',
            json: orderItem
        }, function (error, response, body) {
            let obj = getObjectFromBody(body);
            testOrderItemRecord(falseId, orderItem, response, obj);

            createdOrderItemId = obj.id;

            request('http://localhost:3000/orders/'+createdOrderId+'/items/'+createdOrderItemId, function (errorNext, responseNext, bodyNext) {
                let obj = getObjectFromBody(bodyNext);
                testOrderItemRecord(falseId, orderItem, responseNext, obj);
                done();
            });
        });
    });

    it('Add order\'s item (wrong size)', function (done) {
        const falseId = 999999;

        let orderItem = {
            id: falseId,
            orderId: createdOrderId,
            pizzaId: 3,
            sizeId: 9,
            quantity: 2
        };

        request.post({
            url: 'http://localhost:3000/orders/'+createdOrderId+'/items',
            json: orderItem
        }, function (error, response, body) {
            let obj = getObjectFromBody(body);
            expect(response.statusCode).to.equal(500);
            expect(obj.errorObject.name).to.equal("SequelizeForeignKeyConstraintError");
            done();
        });
    });

    it('Add order\'s item (wrong pizza id)', function (done) {
        const falseId = 999999;

        let orderItem = {
            id: falseId,
            orderId: createdOrderId,
            pizzaId: 100,
            sizeId: 1,
            quantity: 2
        };

        request.post({
            url: 'http://localhost:3000/orders/'+createdOrderId+'/items',
            json: orderItem
        }, function (error, response, body) {
            let obj = getObjectFromBody(body);
            expect(response.statusCode).to.equal(500);
            expect(obj.errorObject.name).to.equal("SequelizeForeignKeyConstraintError");
            done();
        });
    });

    it('Add order\'s item (wrong data)', function (done) {
        const falseId = 999999;

        let orderItem = {
            id: falseId,
            orderId: createdOrderId,
            pizzaId: "sdads23",
            sizeId: "docsi93",
            quantity: "wqdqd"
        };

        request.post({
            url: 'http://localhost:3000/orders/'+createdOrderId+'/items',
            json: orderItem
        }, function (error, response, body) {
            expect(response.statusCode).to.equal(422);
            done();
        });
    });

    it('Delete order item', function (done) {
        request.delete('http://localhost:3000/orders/'+createdOrderId+'/items/'+createdOrderItemId, function (error, response, body) {
            let obj = null;
            if(typeof body == "string") {
                obj = JSON.parse(body);
            } else {
                obj = body;
            }

            expect(response.statusCode).to.equal(200);
            expect(obj.count).to.equal(1);
            done();
        });
    });

    it('Update created order item', function (done) {
        const falseId = 999999;

        const newFalseId = 88888;

        let orderItem = {
            id: falseId,
            orderId: createdOrderId,
            pizzaId: 1,
            sizeId: 2,
            quantity: 5
        };

        let updatedOrderItem = {
            id: newFalseId,
            orderId: createdOrderIdForTest,
            pizzaId: 3,
            sizeId: 1,
            quantity: 10
        };
        request.post({
            url: 'http://localhost:3000/orders/'+createdOrderId+"/items",
            json: orderItem
        }, function (error, response, body) {
            let obj = getObjectFromBody(body);
            testOrderItemRecord(falseId, orderItem, response, obj);

            createdOrderItemId = obj.id;

            request.put({
                url: 'http://localhost:3000/orders/'+createdOrderId+"/items/"+createdOrderItemId,
                json: updatedOrderItem
            }, function (errorNext, responseNext, bodyNext) {
                let obj = getObjectFromBody(bodyNext);
                updatedOrderItem.orderId = createdOrderId;
                testOrderItemRecord(newFalseId, updatedOrderItem, responseNext, obj);
                done();
            });
        });
    });
});