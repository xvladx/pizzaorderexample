/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = pgm => {
    const baseTableDefinition = {
        id: 'id',
        name: {type: 'varchar(100)', notNull: true},
        description: {type: 'varchar(500)'}
    };

    pgm.createTable('sizes', baseTableDefinition);
    pgm.createTable('pizzas', baseTableDefinition);
    pgm.createTable('pizzas_sizes', {
        id: 'id',
        pizzaId: {type: 'integer', notNull: true, references: 'pizzas(id)'},
        sizeId: {type: 'integer', notNull: true, references: 'sizes(id)'},
    });

    let statusTableDefinition = baseTableDefinition;
    statusTableDefinition["order"] = {type: 'smallint', default: 0, notNull: true}
    pgm.createTable('statuses', statusTableDefinition);
    pgm.createTable('customers', {
        id: 'id',
        name: {type: 'varchar(100)', notNull: true},
        address: {type: 'varchar(2000)', notNull: true}
    });
    pgm.createTable('orders', {
        id: 'id',
        customerId: {type: 'integer', notNull: true, references: 'customers(id)'},
        statusId: {type: 'integer', notNull: true, references: 'statuses(id)'},
        date: {type: 'date', notNull: true, default: pgm.func("CURRENT_DATE")}
    });
    pgm.createTable('order_items', {
        id: 'id',
        orderId: {type: 'integer', notNull: true, references: 'orders(id)'},
        pizzaId: {type: 'integer', notNull: true, references: 'pizzas(id)'},
        sizeId: {type: 'integer', notNull: true, references: 'sizes(id)'},
        quantity: {type: 'integer', notNull: true}
    });

    pgm.sql(`INSERT INTO sizes ("name", "description") VALUES ('small', 'Default small pizza'), 
                                                 ('medium', 'Default medium pizza'), ('large', 'Default large pizza')`);
    pgm.sql(`INSERT INTO pizzas ("name", "description") VALUES ('Salami', 'Delicious pizza with salami'), 
                                                 ('Marinara', 'Tasty marinara'), ('Margarita', 'Pizza without meat but cheese')`);
    pgm.sql(`INSERT INTO pizzas_sizes ("pizzaId", "sizeId") VALUES (1, 1), (1, 2), (1, 3), 
                                                     (2, 1), (2, 3), 
                                                     (3, 2), (3, 3)`);
    pgm.sql(`INSERT INTO statuses ("name", "description", "order") VALUES ('new', 'Order was created just recently', 0), 
                                                    ('preparing', 'Your pizza is in cooking process', 1), 
                                                    ('delivering', 'Pizza is on the way to you', 2), 
                                                    ('delivered', 'You order was already delivered. Enjoy :)', 3)`);
    pgm.sql(`INSERT INTO customers ("name", "address") VALUES ('Vlad', 'Germany, Defaultstrasse 99, 68000'), 
                                                 ('John', 'Poland, Default ulica 12, 123456'), 
                                                 ('Nick', 'Germany, yet another sample address')`);
};

exports.down = pgm => {
    pgm.dropTable('sizes');
    pgm.dropTable('pizzas');
    pgm.dropTable('pizzas_sizes');
    pgm.dropTable('statuses');
    pgm.dropTable('customers');
    pgm.dropTable('orders');
    pgm.dropTable('order_items');
};
